from django.urls import path

from .views import api_list_shoes, api_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes_rest/<int:id>/", api_shoes, name="api_shoes"),
]
