from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=200)
 
 
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    image = models.URLField(null=True, blank=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
        )



