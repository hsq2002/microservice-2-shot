from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Hats(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200, null=True, blank=True)
    picture_url = models.URLField(max_length=200, null=True)
    fabric = models.CharField(max_length=200, null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="+",
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return f"{self.name}"
