from django.shortcuts import render
from common.json import ModelEncoder
from .models import LocationVO
from .models import Hats
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "color",
        "picture_url",
        "fabric",
        "location"
    ]

    encoders = {
        "location": LocationVODetailEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        try:
            hats = Hats.objects.create(**content)
            return JsonResponse(
                hats,
                encoder=HatsDetailEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse (str(e), safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_hats(request):
    if request.method == "GET":
        hats = Hats.objects.get(id=id)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
