import React, {useEffect, useState} from "react";

function HatsForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState("");
    const [color, setColor] = useState("");
    const [picture_url, setPictureURL] = useState("");
    const [fabric, setFabric] = useState("");
    const [location, setLocation] = useState("");

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
          console.log(data)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.color = color;
        data.picture_url = picture_url;
        data.fabric = fabric;
        data.location = location;


    const hatsurl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
    };

        const response = await fetch(hatsurl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setName(" ");
            setColor(" ");
            setPictureURL(" ");
            setFabric(" ");
            setLocation(" ");


        }
    }

    useEffect(() => {
        fetchData();
        }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureURLChange} placeholder="picture" required type="picture" name="picture" id="picture" className="form-control" />
                <label htmlFor="Max_presentations">Picture URL</label>
              </div>
              <div onChange={handleFabricChange} className="form-floating mb-3">
                <input placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location"  className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return(
                        <option key={location.href} value={location.href}>
                        {location.closet_name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}


export default HatsForm;
