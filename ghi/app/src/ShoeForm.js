import React, { useEffect, useState } from "react";

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [modelName, setModelName] = useState('')
    const [color, setColor] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [image, setImage] = useState('')
    const [bin, setBin] = useState('')

    const fetchData = async () => {
        const binsUrl = 'http://localhost:8100/api/bins/';
        const response = await fetch(binsUrl);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)  
            console.log(data);    
        }
    }
    useEffect(() => {
        fetchData();
      }, []);
    
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = modelName;
        data.color = color;
        data.image = image;
        data.bin = bin;
        data.manufacturer = manufacturer; 


        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            // console.log(newShoe)
            setModelName(' ');
            setColor(' ');
            setManufacturer(' ');
            setImage(' ');
            setBin(' ');

        }
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }
    const handleImageChange = (event) => {
        const value = event.target.value;
        setImage(value)
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value)
    }

    return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={modelName} onChange={handleModelNameChange} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={image} onChange={handleImageChange} placeholder="Picture" required type="text" name="image" id="image" className="form-control" />
              <label htmlFor="image">Picture</label>
            </div>
            <div className="mb-3">
              <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin </option>
                {bins.map(bin => {
                return (
                    <option key={bin.href} value={bin.href}>
                        {bin.closet_name}
                    </option>
                );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;

