import React, { useState, useEffect } from "react"

function ShoesList() {
    
    const [shoes, setShoes] = useState([]);
    const LoadShoes = async () => {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch (url)
        console.log(response);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }
    useEffect(()=> {
        LoadShoes();
      }, [])

    return (
        <table className="tabel table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>image</th>
                </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                return(
                    <tr key={shoe.href} >
                        <td> {shoe.manufacturer} </td>
                        <td> {shoe.name} </td>
                        <td> {shoe.color} </td>
                        <td> {shoe.image} </td>
                    </tr>
                );
            }
                )}
            </tbody>
        </table>
    )
        }
export default ShoesList;
