import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatsForm from "./HatsForm";
import HatsList from "./HatsList";
import Nav from './Nav';
import ShoeForm from './ShoeForm'
import ShoesList from './ShoesList'

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsForm />} />
          <Route path="/hatlist" element={<HatsList />} />
          <Route path="/shoes" element={<ShoeForm />} />
          <Route path="/shoeslist" element={<ShoesList/>}/>
        </Routes>
      </div>
      {/* {<HatsList hats={props.hats} />} */}
    </BrowserRouter>
  );
}

export default App;
