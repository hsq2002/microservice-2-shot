import React, {useState, useEffect} from "react";
function HatsList() {
    const [hats, setHats] = useState([]);
    const LoadHats = async () => {

        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url)
        console.log(response);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(() => {
        LoadHats()
    },[])
    return (
        <>
        <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>id</th>
                <th>color</th>
                <th>picture_url</th>
                <th>fabric</th>
                <th>location</th>
              </tr>
            </thead>
            <tbody>
              {/* for (let attendee of props.attendees) {
                <tr>
                  <td>{ attendee.name }</td>
                  <td>{ attendee.conference }</td>
                </tr>
              } */}
              {hats?.map(hat => {
                return (
                  <tr key={hat.location.href}>
                    <td>{ hat.name }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.picture_url }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.location.closet_name }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
      );
    }

export default HatsList;
